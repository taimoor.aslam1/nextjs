import { useRouter } from "next/router";
import Layout from "../../components/sections/Layout";
import styles from "../../styles/Home.module.scss";
import Head from "next/head";
// import data
import data from "../../data/portfolio.json";

export async function getStaticProps({ params }) {
  return {
    props: {
      prodata: data.find((data) => {
        return data.id.toString() === params.project;
      }),
    }, // will be passed to the page component as props
  };
}
export async function getStaticPaths() {
  return {
    paths: [
      { params: { project: "1" } },
      { params: { project: "2" } },
      { params: { project: "3" } },
    ],
    fallback: true, // false or 'blocking'
  };
}

const Portfolio = (props) => {
  const router = useRouter();
  const { project } = router.query;
  //   console.log("props", props);
  if (router.isFallback) {
    return (
      <>
        <Layout>
          <div className={styles.main}>
            <h1
              className={`text-slate-900 font-extrabold text-4xl sm:text-5xl lg:text-6xl tracking-tight text-center dark:text-white`}
            >
              Loading...
            </h1>
          </div>
        </Layout>
      </>
    );
  }
  return (
    <>
      <Head>
        <title>{props.prodata.portfolio}</title>
      </Head>
      <div className={styles.container}>
        <Layout>
          <div className={styles.main}>
            <h1
              className={`text-slate-900 font-extrabold text-4xl sm:text-5xl lg:text-6xl tracking-tight text-center dark:text-white`}
            >
              {props.prodata.portfolio}
            </h1>
            <button
              className="my-6 bg-slate-900 hover:bg-slate-700 focus:outline-none focus:ring-2 focus:ring-slate-400 focus:ring-offset-2 focus:ring-offset-slate-50 text-white font-semibold h-12 px-6 rounded-lg w-full flex items-center justify-center sm:w-auto dark:bg-sky-500 dark:highlight-white/20 dark:hover:bg-sky-400"
              onClick={() => router.push("/")}
            >
              Go to Home
            </button>
          </div>
        </Layout>
      </div>
    </>
  );
};

export default Portfolio;
