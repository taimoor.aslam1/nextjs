import Link from "next/link";
import Head from "next/head";
import Layout from "../components/sections/Layout";
import CardList from "../components/sections/CardList";
import styles from "../styles/Home.module.scss";
import data from "../data/portfolio.json";

export async function getStaticProps(context) {
  return {
    props: {
      data,
    }, // will be passed to the page component as props
  };
}

export default function Home(props) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Welcome to Nxto</title>
      </Head>
      <Layout>
        <div className={styles.main}>
          <h1
            className={`text-slate-900 font-extrabold text-4xl sm:text-5xl lg:text-6xl tracking-tight text-center dark:text-white`}
          >
            Welcome to NxTo!
          </h1>

          <p className="mt-6 text-lg text-slate-600 text-center max-w-2xl mx-auto dark:text-slate-400">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime
            mollitia, molestiae quas vel sint commodi repudiandae consequuntur
            voluptatum laborum numquam
          </p>
          <Link href="portfolio/get-started">
            <button className="my-6 bg-slate-900 hover:bg-slate-700 focus:outline-none focus:ring-2 focus:ring-slate-400 focus:ring-offset-2 focus:ring-offset-slate-50 text-white font-semibold h-12 px-6 rounded-lg w-full flex items-center justify-center sm:w-auto dark:bg-sky-500 dark:highlight-white/20 dark:hover:bg-sky-400">
              Get Started
            </button>
          </Link>
          <CardList data={props.data} />
        </div>
      </Layout>
    </div>
  );
}
