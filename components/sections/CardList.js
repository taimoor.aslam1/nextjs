import Image from "next/image";
import Card from "../common/Card";

const CardList = ({ data }) => {
  return (
    <div className="relativ pt-16 pb-20 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-8">
      <div className="relative max-w-7xl mx-auto">
        <div className="text-center">
          <h2 className="text-3xl tracking-tight font-extrabold text-black dark:text-white sm:text-4xl">
            Our Portfolio
          </h2>
          <p className="mt-3 max-w-2xl mx-auto text-xl text-slate-600 dark:text-slate-400 sm:mt-4">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa
            libero labore natus atque, ducimus sed.
          </p>
        </div>
        <div className="mt-12 max-w-lg mx-auto grid gap-5 lg:grid-cols-3 lg:max-w-none">
          {data.map((post) => (
            <Card key={post.id} post={post} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default CardList;
