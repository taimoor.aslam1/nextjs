import Image from "next/image";
import styles from "../../styles/Card.module.scss";
import Link from "next/link";

const Card = ({ post }) => {
  return (
    <div className="flex flex-col rounded-lg shadow-lg overflow-hidden">
      <div className={styles.cardimgWapper}>
        <Image
          className="h-full w-full object-cover image"
          src={post.imgUrl}
          alt="Portfolio Image"
          height={200}
          width={400}
        />
      </div>
      <div className="flex-1 bg-white p-6 flex flex-col justify-between dark:bg-slate-800">
        <div className="flex-1">
          <Link href={`portfolio/${post.id}`} className="block mt-2">
            <a>
              <p className="text-xl font-semibold text-gray-900 dark:text-white">
                {post.portfolio}
              </p>
              <p className="mt-3 text-base text-gray-500 dark:text-gray-400">
                {post.desc}
              </p>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Card;
